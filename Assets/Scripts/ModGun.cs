﻿using UnityEngine;
using System.Collections;

public class ModGun : MonoBehaviour {

	public GameObject barrel;
	public Transform barrelSpawn; // Barrel spawn point

	// Gun Mods
	public float fireRate; // Fire rate

	[Range(1, 20)]
	public int barrels = 0; // Amount of barrels

	[Range(0.0f, 1.0f)]
	public float barrelRadius = 0.4f;

	public float spread; // Projectile spread

	// Private
	private float nextFire;
	private int barrelCount; // Actual barrel count

	void Start () {

	}

	// Update is called once per frame
	void Update () {

		SpawnBarrel();
		
		// Old fire code
		// if(Input.GetButton("Fire1") && Time.time > nextFire) {
			// nextFire = Time.time + fireRate;

			// for(int i = 0; i < barrels; i++) {
			// 	shotSpawn.Rotate(0, i-(barrels/2*spread), 0);
			// 	Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
			// }

		// }
	}

	void SpawnBarrel() {

		// Reinstantiate all barrels when number of barrels change
		if(barrelCount != barrels) {

			// Destroy barrels
			for(int i=0; i<barrelCount; i++) {
				Destroy(GameObject.Find("Barrel_"+i));
			}

			barrelCount = 0;

			// Instantiate barrels
			for(int i=0; i<barrels ; i++) {

				Vector3 pos = calculateCircle(barrelSpawn.position, barrelRadius, i);
				GameObject barrelClone = Instantiate(barrel, pos, barrelSpawn.rotation) as GameObject;

				barrelClone.name = "Barrel_" + i;
				barrelClone.transform.parent = GameObject.Find("BarrelSpawn").transform;
				barrelCount++;
			}
		}

	}

	// Calculate a circle for barrel spawning
	Vector3 calculateCircle (Vector3 center, float radius, int i){
    	Vector3 pos;
		float tmpPos = 0;
		
		tmpPos = ((360/barrels)*(i+1));

		if(barrels == 1) {
			pos.x = center.x;
			pos.y = center.y;
			pos.z = center.z;
			
		}
		else {
			pos.x = center.x + radius * Mathf.Sin(tmpPos * Mathf.Deg2Rad);
			pos.y = center.y + radius * Mathf.Cos(tmpPos * Mathf.Deg2Rad);
			pos.z = center.z;	
		}

		return pos;
     }
}
