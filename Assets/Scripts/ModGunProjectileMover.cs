﻿using UnityEngine;
using System.Collections;

public class ModGunProjectileMover : MonoBehaviour {

	public float speed;
	public float lifeTime;

	// Use this for initialization
	void Start () {
		Rigidbody rb = GetComponent<Rigidbody>();
		rb.velocity = transform.forward * speed;
		Destroy (gameObject, lifeTime);
	}
}
